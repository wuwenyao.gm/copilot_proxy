//This file contains the Config class and related methods.
import 'package:copilot_proxy/token_manager.dart';
import 'package:copilot_proxy/utils/json_util.dart';

class Config {
  final String listenIp;
  final int listenPort;
  final List<NewTokenData> targetList;
  final String tokenSalt;
  final String adminPassword;
  final int copilotDebounce;
  String? configFilePath;

  Config({
    required this.listenIp,
    required this.listenPort,
    required this.targetList,
    required this.tokenSalt,
    required this.adminPassword,
    required this.copilotDebounce,
  });

  factory Config.fromJson(JsonMap configMap) {
    final JsonList targetList = configMap['targetList'] ?? [];
    return Config(
      listenIp: configMap['listenIp'] ?? '0.0.0.0',
      listenPort: configMap['listenPort'] ?? 8080,
      targetList: targetList.map((e) => NewTokenData.fromJson(e)).toList(),
      tokenSalt: configMap['tokenSalt'] ?? 'default_salt',
      adminPassword: configMap['adminPassword'] ?? 'default_admin_password',
      copilotDebounce: configMap['copilotDebounce'] ?? 1000,
    );
  }
}
