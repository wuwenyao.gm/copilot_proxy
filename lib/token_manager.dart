//This file contains the TokenManager class and related methods.
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:copilot_proxy/common.dart';
import 'package:copilot_proxy/extension.dart';
import 'package:copilot_proxy/header_manager.dart';
import 'package:copilot_proxy/utils/utils.dart';

class Lock {
  int _useCount = 0;
  Completer<void> _lock = Completer<void>()..complete();

  void lock() {
    _useCount++;
    _lock = Completer<void>();
  }

  void unlock() {
    _useCount--;
    _lock.complete();
  }

  Future<void> wait() => _lock.future;

  bool get isBusy => _useCount > 0 || !_lock.isCompleted;
}

class NewTokenData {
  final Uri? authUrl;
  final String authToken;
  final String type;
  final String? httpProxyAddr;
  final Uri? codexUrl;
  final Uri? chatUrl;
  String? requestToken;
  DateTime? _expiry;
  int _invalidatedCount = 0;
  int _useCount = 0;

  final _lock = Lock();
  final client = HttpClient();

  NewTokenData({
    required this.authToken,
    required this.type,
    this.authUrl,
    this.httpProxyAddr,
    this.codexUrl,
    this.chatUrl,
  }) {
    if (httpProxyAddr == null) return;
    setHttpProxy(httpProxyAddr!);
  }

  String get hash {
    return md5String('$type$authToken$codexUrl$chatUrl');
  }

  void setHttpProxy(String httpProxyAddr) {
    final proxy = 'PROXY $httpProxyAddr';
    client.findProxy = (uri) => proxy;
  }

  void invalidated() => _invalidatedCount++;

  bool get isInvalidated => _invalidatedCount >= 3;

  bool isExpiry(DateTime now) {
    final time = _expiry;
    return time == null || now.isAfter(time);
  }

  ///设置过期
  void setExpiry() => _expiry = null;

  void lock() {
    _useCount++;
    _lock.lock();
  }

  void unlock() => _lock.unlock();

  Future<void> wait() => _lock.wait();

  bool get isBusy => _lock.isBusy;

  void updateRequestToken(JsonMap bodyJson) {
    final token = bodyJson['token'];
    final expiryAt = bodyJson['expires_at'];
    requestToken = token;
    if (expiryAt == null) return;
    _expiry = DateTime.fromMillisecondsSinceEpoch(expiryAt * 1000);
    _useCount = 0;
  }

  factory NewTokenData.fromGHU(githubToken) {
    return NewTokenData(
      authUrl: Uri.parse('https://api.github.com/copilot_internal/v2/token'),
      authToken: githubToken,
      type: 'copilot',
      codexUrl: Uri.parse('https://copilot-proxy.githubusercontent.com/v1/engines/copilot-codex/completions'),
      chatUrl: Uri.parse('https://api.githubcopilot.com/chat/completions'),
    );
  }

  factory NewTokenData.fromJson(JsonMap tokenMap) {
    final String? codexUrl = tokenMap['codexUrl'];
    final String? chatUrl = tokenMap['chatUrl'];
    final tokenData = NewTokenData(
      authUrl: Uri.parse(tokenMap['authUrl']),
      authToken: tokenMap['authToken'],
      type: tokenMap['type'] ?? 'copilot',
      httpProxyAddr: tokenMap['httpProxyAddr'],
      codexUrl: codexUrl == null ? null : Uri.parse(codexUrl),
      chatUrl: chatUrl == null ? null : Uri.parse(chatUrl),
    );
    tokenData.requestToken = tokenMap['requestToken'];
    tokenData._invalidatedCount = tokenMap['invalidatedCount'] ?? 0;
    tokenData._useCount = tokenMap['useCount'] ?? 0;
    tokenData._expiry = DateTime.tryParse(tokenMap['expiry'] ?? '');
    return tokenData;
  }

  JsonMap toJson() {
    return {
      'authUrl': authUrl.toString(),
      'authToken': authToken,
      'type': type,
      'httpProxyAddr': httpProxyAddr,
      'codexUrl': codexUrl.toString(),
      'chatUrl': chatUrl.toString(),
      'expiry': _expiry?.toIso8601String(),
      'requestToken': requestToken,
      'invalidatedCount': _invalidatedCount,
      'useCount': _useCount,
    };
  }
}

class HttpProxy {
  final String addr;
  int useCount = 0;

  HttpProxy(this.addr);
}

class TokenManager {
  TokenManager._();

  static final TokenManager instance = TokenManager._();

  factory TokenManager() => instance;

  final List<NewTokenData> _codexTokens = [];

  final List<NewTokenData> _chatTokens = [];

  Future<void> loadTokenData() async {
    await _loadTokenDataByDB();
    await _loadTokenDataByConfig();
  }

  Future<void> _loadTokenDataByDB() async {
    final tokenList = await getAllTokenData();
    for (final tokenMap in tokenList) {
      addTokenData(NewTokenData.fromJson(tokenMap));
    }
  }

  Future<void> _loadTokenDataByConfig() async {
    final targetList = List.of(config.targetList);
    final targetMap = Map<String, NewTokenData>.fromIterable(targetList, key: (e) => e.hash);
    for (final e in _codexTokens) {
      final remove = targetMap.remove(e.hash);
      targetList.remove(remove);
    }
    for (final e in _chatTokens) {
      final remove = targetMap.remove(e.hash);
      targetList.remove(remove);
    }
    for (final tokenData in targetList) {
      addTokenData(tokenData);
    }
  }

  void addTokenData(NewTokenData tokenData) {
    if (tokenData.codexUrl != null) _codexTokens.add(tokenData);
    if (tokenData.chatUrl != null) _chatTokens.add(tokenData);
  }

  Future<NewTokenData?> _findTokenData(List<NewTokenData> tokens) async {
    final copilotTokens = tokens.where((e) => e.type == 'copilot').toList();
    final openAITokens = tokens.where((e) => e.type == 'openai').toList();
    final [copilotTokenData, openAITokenData] = await Future.wait([
      _findCopilotTokenData(copilotTokens),
      _findOpenAITokenData(openAITokens),
    ]);
    if (copilotTokenData != null && openAITokenData != null) {
      final bothBusy = copilotTokenData.isBusy && openAITokenData.isBusy;
      final bothNowBusy = !copilotTokenData.isBusy && !openAITokenData.isBusy;
      if (bothBusy || bothNowBusy) {
        if (copilotTokenData._useCount > openAITokenData._useCount) return openAITokenData;
        return copilotTokenData;
      } else {
        if (copilotTokenData.isBusy) return openAITokenData;
        return copilotTokenData;
      }
    }
    return copilotTokenData ?? openAITokenData;
  }

  Future<NewTokenData?> _findOpenAITokenData(List<NewTokenData> tokens) async {
    final busyTokens = <NewTokenData>[];
    final availableTokens = <NewTokenData>[];
    for (final tokenData in tokens) {
      //token is invalid
      if (tokenData.isInvalidated) continue;
      //token is busy
      if (tokenData.isBusy) {
        busyTokens.add(tokenData);
        continue;
      }
      if (tokenData.requestToken != null) {
        availableTokens.add(tokenData);
        continue;
      }
      tokenData.requestToken = tokenData.authToken;
      await saveNewTokenData(tokenData);
      availableTokens.add(tokenData);
    }
    if (availableTokens.isNotEmpty) {
      return availableTokens.minBy((e) => e._useCount);
    }
    if (busyTokens.isNotEmpty) {
      return busyTokens.minBy((e) => e._useCount);
    }
    return null;
  }

  Future<NewTokenData?> _findCopilotTokenData(List<NewTokenData> tokens) async {
    final now = DateTime.now();
    final busyTokens = <NewTokenData>[];
    final availableTokens = <NewTokenData>[];
    for (final tokenData in tokens) {
      //token is invalid
      if (tokenData.isInvalidated) continue;
      //token is busy
      if (tokenData.isBusy) {
        busyTokens.add(tokenData);
        continue;
      }
      if (!tokenData.isExpiry(now) && tokenData.requestToken != null) {
        availableTokens.add(tokenData);
        continue;
      }
      //token is expiry or copilotToken not exists
      tokenData.lock();
      final resp = await _getRequestTokenResponse(tokenData);
      if (resp.statusCode != HttpStatus.ok) {
        log('refreshCopilot: fail');
        tokenData._invalidatedCount++;
        tokenData.unlock();
        continue;
      }
      log('refreshCopilot: ok');
      final bodyJson = jsonDecode(await resp.getBody());
      tokenData.updateRequestToken(bodyJson);
      await saveNewTokenData(tokenData);
      tokenData.unlock();
      availableTokens.add(tokenData);
    }
    if (availableTokens.isNotEmpty) {
      return availableTokens.minBy((e) => e._useCount);
    }
    if (busyTokens.isNotEmpty) {
      return busyTokens.minBy((e) => e._useCount);
    }
    return null;
  }

  Future<HttpClientResponse> _getRequestTokenResponse(NewTokenData tokenData) async {
    final request = await tokenData.client.getUrl(tokenData.authUrl!);
    final headers = HeaderManager.instance.getHeaders(tokenData.authUrl!.path);
    final customHeaders = {
      ...headers,
      HttpHeaders.authorizationHeader: ['token ${tokenData.authToken}'],
    };
    request.setHeaders(customHeaders);
    return request.close();
  }

  Future<T?> useCodexTokenData<T>(
    String? username,
    Future<T> Function(NewTokenData? tokenData) callback,
  ) {
    return _useNewTokenData(username, _codexTokens, callback);
  }

  Future<T?> useChatTokenData<T>(
    String? username,
    Future<T> Function(NewTokenData? tokenData) callback,
  ) {
    return _useNewTokenData(username, _chatTokens, callback);
  }

  Future<T?> _useNewTokenData<T>(
    String? username,
    List<NewTokenData> tokens,
    Future<T> Function(NewTokenData? tokenData) callback,
  ) async {
    final completer = Completer<T?>();
    //使用debouncer防止多次请求
    TokenDebouncer.run(username, () async {
      final tokenData = await _findTokenData(tokens);
      if (tokenData == null) {
        completer.complete(await callback(null));
        return;
      }
      while (tokenData.isBusy) {
        await tokenData.wait();
      }
      tokenData.lock();
      if (tokenData._useCount > 100000) {
        final minUseToken = tokens.minBy((e) => e._useCount)._useCount;
        for (var e in tokens) {
          e._useCount -= minUseToken;
        }
      }
      final result = await callback(tokenData);
      tokenData.unlock();
      completer.complete(result);
    });
    return completer.future;
  }
}

class TokenDebouncer {
  static final _usernameDebouncerMap = <String, TokenDebouncer>{};
  static final duration = Duration(milliseconds: config.copilotDebounce);
  Timer? _timer;

  static void run(String? username, Future<void> Function() action) {
    final debouncer = _usernameDebouncerMap[username ?? ''] ??= TokenDebouncer();
    debouncer._timer?.cancel();
    debouncer._timer = Timer(duration, () async {
      await action();
      _usernameDebouncerMap.remove(username);
    });
  }
}
