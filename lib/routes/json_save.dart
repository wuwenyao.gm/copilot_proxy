import 'package:copilot_proxy/context.dart';
import 'package:copilot_proxy/http_server.dart';

Future<void> postJsonSave(Context context) async {
  await saveAllData();
  context.noContent();
}
