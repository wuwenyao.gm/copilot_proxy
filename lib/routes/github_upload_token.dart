import 'package:copilot_proxy/context.dart';
import 'package:copilot_proxy/token_manager.dart';

Future<void> postGithubUploadToken(Context context) async {
  TokenManager.instance.addTokenData(NewTokenData.fromGHU(context['githubToken']));
  context.noContent();
}
